import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class Header extends Component {
    render() {
        return (
          <div className="Header">
            <div className="head hidden-sm hidden-xs"><div className="instagram"><img src="img/instagram.png" height="32" alt="logo" />#hotdogs</div></div>
              <div className="topnav" id="myTopnav">
              <div className="nav-center">
                <Link to='/first_react_app/'>Menu</Link>
                <a href="#1">Catering</a>
                <a href="#1">About us</a>
                <Link to='/first_react_app/contact'>Contact</Link>
                <a href="#1" className="icon">&#9776;</a>
              </div>
            </div>
          </div>
        );
      }
}
