import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import './App.css';

import Header from './Header';
import Footer from './Footer';
import HotDogs from './HotDogs';
import Contact from './Contact';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
            <Switch>
              <Route path="/first_react_app/contact" component={Contact} />
              <Route path="/first_react_app/" component={HotDogs} />
            </Switch>
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
