import React, { Component } from 'react';

export default class HotDogs extends Component {

  state = {
    hotDogs: []
  }

  async componentDidMount() {
    try {
      const res = await fetch('https://formula-test-api.herokuapp.com/menu');
      const hotDogs = await res.json();
      console.log(hotDogs);
      this.filterByExpiration(hotDogs);
      this.setState({
        hotDogs: hotDogs
      })
    } 
    catch (e) {
      console.log(e);
    }
  }

  filterByExpiration = (items) => {
    let today = new Date();
    let i = items.length
    while (i--) {  
      //converting data format 
      let itemDate = items[i].expirationDate.split('-');
      let itemDateEdit = itemDate.slice(0, 2);
      itemDateEdit.unshift(itemDate[2]);
      itemDate = itemDateEdit.join('-');
      let newItemDate = new Date(itemDate)
      //removing all items older than today's date
      if (today > newItemDate) { 
        items.splice(i, 1);
      } 
  }
    return items;
  };

  render() {
    return (
      <div className="hotDogsBody">
        <HotDogsHeader />
        {this.state.hotDogs.map((hotDog, i) => <HotDogList key={hotDog.id} 
                                                      id={i} 
                                                      name={hotDog.name} 
                                                      description={hotDog.description}
                                                      backgroundURL={hotDog.backgroundURL} />)}
      </div>
    );
  }
}

class HotDogsHeader extends Component {
  render() {
    return (
      <div className="HotDogs">
        <section className="hot-dog">
          <img src="img/hot-dog.jpg" alt="hot-dog" /><br /><br /> 
          <h1>Dirty Dogs serves all-beef, vegan and<br /> vegatagian hot dogs.</h1>
          <a href="#1" className="btn btn-info btn-blue" role="button">More Dogs'n Make Em Hot</a>
        </section>
      </div>
    );
  }
}

class HotDogList extends Component {
  render() {
    if (this.props.id % 2) {
      return (
        <div className="HotDogs text-left">
          <HotDogRight name={this.props.name} description={this.props.description} backgroundURL={this.props.backgroundURL}/>
      </div>
      );
    }
    else {
      return (
        <div className="HotDogs text-left">
          <HotDogLeft name={this.props.name} description={this.props.description} backgroundURL={this.props.backgroundURL}/>
      </div>
      );
    }
  }
}

class HotDogLeft extends Component {
  render() {
    return (
      <div className="HotDog">
        <div className="row">
          <div className="col-md-6 block-text">
            <div className="row">
                <div className="col">
                  <div className="line hidden-sm hidden-xs hot-dog-line"></div>
                </div>
                <div className="col-md-11 col-xs-12 hot-dog-text">
                  <h3>{this.props.name}</h3>
                  <p>{this.props.description}</p>
                </div>
            </div>
          </div>
        <div className="col-md-6">
          <div className="block-img" style={{background: `url(${this.props.backgroundURL}) no-repeat center center`, backgroundSize: 'cover'}}></div>
        </div>
      </div>
    </div>
    );
  }
}

class HotDogRight extends Component {
  render() {
    return (
      <div className="HotDog">
        <div className="row flex">
          <div className="col-md-6">
            <div className="block-img" style={{background: `url(${this.props.backgroundURL}) no-repeat center center`, backgroundSize: 'cover'}}></div>
          </div>
          <div className="col-md-6 block-text flex-first flex-md-unordered">
            <div className="row">
                <div className="col">
                  <div className="line hidden-sm hidden-xs hot-dog-line"></div>
                </div>
                <div className="col-11 hot-dog-text">
                  <h3>{this.props.name}</h3>
                  <p>{this.props.description}</p>
                </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}